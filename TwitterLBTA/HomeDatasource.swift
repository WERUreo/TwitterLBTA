//
//  HomeDatasource.swift
//  TwitterLBTA
//
//  Created by Keli'i Martin on 2/25/17.
//  Copyright © 2017 WERUreo. All rights reserved.
//

import LBTAComponents

class HomeDatasource: Datasource
{
    let users: [User] =
    {
        let brianUser = User(name: "Keli'i Martin", username: "@werureo", bioText: "iOS developer", profileImage: #imageLiteral(resourceName: "profile_image"))
        let rayUser = User(name: "Ray Wenderlich", username: "@rwenderlich", bioText: "Ray Wenderlich is an iPhone developer and tweets on topics related to iPhone, software, and gaming.  Check out our conference.", profileImage: #imageLiteral(resourceName: "ray_profile_image"))
        let kindleCourseUser = User(name: "Kindle Course", username: "@kindleCourse", bioText: "This recently released course on https://videos.letsbuildthatapp.com/basic-training provides some excellent guidance on how to use UITableView and UICollectionView. This recently released course on https://videos.letsbuildthatapp.com/basic-training provides some excellent guidance on how to use UITableView and UICollectionView.", profileImage: #imageLiteral(resourceName: "profile_image"))
        return [brianUser, rayUser, kindleCourseUser]
    }()

    let tweets = ["tweet1", "tweet2"]

    override func numberOfItems(_ section: Int) -> Int
    {
        if section == 1 { return tweets.count }
        return users.count
    }

    override func item(_ indexPath: IndexPath) -> Any?
    {
        return users[indexPath.item]
    }

    override func cellClasses() -> [DatasourceCell.Type]
    {
        return [UserCell.self, TweetCell.self]
    }

    override func headerClasses() -> [DatasourceCell.Type]?
    {
        return [UserHeader.self]
    }

    override func footerClasses() -> [DatasourceCell.Type]?
    {
        return [UserFooter.self]
    }

    override func numberOfSections() -> Int
    {
        return 2
    }
}
