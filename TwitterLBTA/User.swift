//
//  User.swift
//  TwitterLBTA
//
//  Created by Keli'i Martin on 2/26/17.
//  Copyright © 2017 WERUreo. All rights reserved.
//

import Foundation
import UIKit

struct User
{
    let name: String
    let username: String
    let bioText: String
    let profileImage: UIImage
}
